# -*- coding: utf-8 -*-

import kivy

kivy.require('1.5.2')
from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.uix.popup import Popup
import random
from kivy.uix.boxlayout import BoxLayout

signs = ("O", "X")
points = {
    'O': 0,
    'X': 0
}
conditions = ((1, 2, 3), (4, 5, 6), (7, 8, 9),  # Horizontal
              (3, 5, 7), (1, 5, 9),  # Diagonal
              (1, 4, 7), (2, 5, 8), (3, 6, 9))  # Vertical

moves = {
    (0, 3, 6, 27, 30, 33, 54, 57, 60): (0, 1, 2, 9, 10, 11, 18, 19, 20),
    (1, 4, 7, 28, 31, 34, 55, 58, 61): (3, 4, 5, 12, 13, 14, 21, 22, 23),
    (2, 5, 8, 29, 32, 35, 56, 59, 62): (6, 7, 8, 15, 16, 17, 24, 25, 26),
    (9, 12, 15, 36, 39, 42, 63, 66, 69): (27, 28, 29, 36, 37, 38, 45, 46, 47),
    (10, 13, 16, 37, 40, 43, 64, 67, 70): (30, 31, 32, 39, 40, 41, 48, 49, 50),
    (11, 14, 17, 38, 41, 44, 65, 68, 71): (33, 34, 35, 42, 43, 44, 51, 52, 53),
    (18, 21, 24, 45, 48, 51, 72, 75, 78): (54, 55, 56, 63, 64, 65, 72, 73, 74),
    (19, 22, 25, 46, 49, 52, 73, 76, 79): (57, 58, 59, 66, 67, 68, 75, 76, 77),
    (20, 23, 26, 47, 50, 53, 74, 77, 80): (60, 61, 62, 69, 70, 71, 78, 79, 80),
}

bigboard = sorted([move for _, move in moves.iteritems()], key=lambda x: x[0])
game_mode = 0


class MessageBox(Popup):
    def __init__(self, title, message, **kwargs):
        self.title = title
        self.size_hint_x = self.size_hint_y = 0.7
        super(MessageBox, self).__init__(**kwargs)
        self.add_widget(Button(text=message, on_press=lambda x: self.dismiss()))
        self.open()

    @staticmethod
    def checked_message():
        MessageBox('Niedozwolony ruch', u"To pole jest już zajęte!")

    @staticmethod
    def move_not_permitted():
        MessageBox('Niedozwolony ruch', u"Możesz wykonać ruch tylko na żółte pole!")

    @staticmethod
    def draw_message():
        MessageBox('Koniec gry', u"Remis! \n 'O': {0} - {1}: 'X'".format(points['O'], points['X']))

    @staticmethod
    def won_message(player):
        MessageBox('Koniec gry', u"Wygrał gracz {0}! \n 'O': {1} - {2}: 'X'".format(player, points['O'], points['X']))


class GameConfig(Popup):
    def __init__(self, title, **kwargs):
        self.title = title
        self.size_hint_x = self.size_hint_y = 0.7
        super(GameConfig, self).__init__(**kwargs)
        self.auto_dismiss = False

        box = BoxLayout()
        box.add_widget(Button(text="1 vs 1", on_press=lambda x: self.mode(1)))
        box.add_widget(Button(text="1 vs PC", on_press=lambda x: self.mode(2)))
        self.add_widget(box)
        self.open()

    def mode(self, mode):
        global game_mode
        game_mode = mode
        self.dismiss()


class TicTacToe(GridLayout):
    def __init__(self, **kwargs):
        self.cols = 9
        self.rows = 9

        self.board_config = {
            'last': None, 'current_sign': 0, 'won': {}
        }

        super(TicTacToe, self).__init__(**kwargs)

        for position in xrange(self.rows * self.cols):
            button = Button(font_size=100, on_press=self.button_pressed)
            button.position = position
            self.add_widget(button)

        self.colorize_next_move()

    def button_pressed(self, w):
        if w.text:
            MessageBox.checked_message()
            return

        if w.position not in self.next_available_moves():
            MessageBox.move_not_permitted()
            return

        w.text = signs[self.board_config['current_sign']]
        self.board_config['last'] = w.position
        self.after_move()
        if game_mode == 2:
            self.opponent_move()

    def won_child(self, child):
        for board, won in self.board_config['won'].iteritems():
            if child.position in board:
                return won
        return None

    def is_game_won(self):
        g = []
        for board in bigboard:
            if board in self.board_config['won']:
                g.append(self.board_config['won'][board])
            else:
                g.append('')

        for con in conditions:
            if (g[con[0] - 1] == g[con[1] - 1] == g[con[2] - 1]) and g[con[0] - 1] != '':
                return True

    def next_available_moves(self):
        available_moves = []

        for current_moves, next_moves in moves.iteritems():
            if self.board_config['last'] in current_moves:
                for i in next_moves:
                    for child in self.children:
                        if child.position == i:
                            if child.text == '':
                                available_moves.append(child.position)

        if len(available_moves) == 0 or self.board_config['last'] is None:
            for child in self.children:
                if child.text == '':
                    available_moves.append(child.position)

        return available_moves

    def colorize_next_move(self):
        colors = {signs[0]: (255, 0, 0, 255), signs[1]: (0, 0, 255, 255), '': (1, 1, 1, 1)}

        available_moves = self.next_available_moves()
        for child in self.children:
            if child.position in available_moves:
                child.background_color = (244, 34, 1, 1)
            elif child.position == self.board_config['last']:
                child.background_color = (0, 255, 0, 1)
            else:
                won_child = self.won_child(child)
                if won_child is None:
                    child.background_color = colors[child.text]
                else:
                    child.background_color = colors[won_child]

    def after_move(self):
        if len(self.next_available_moves()) == 0:
            MessageBox.draw_message()
            self.reset()

        children = sorted(self.children, key=lambda x: x.position)
        for m, move in moves.iteritems():
            if self.board_config['last'] in move:
                if self.board_config['won'].get(move, None) is None:
                    for con in conditions:
                        a = move[con[0] - 1]
                        b = move[con[1] - 1]
                        c = move[con[2] - 1]
                        d = children[a].text
                        e = children[b].text
                        f = children[c].text
                        if (d == e == f) and d != '':
                            self.board_config['won'][move] = d
                            if self.is_game_won():
                                points[signs[self.board_config['current_sign']]] += 1
                                MessageBox.won_message(signs[self.board_config['current_sign']])
                                self.reset()

        self.board_config['current_sign'] = 0 if self.board_config['current_sign'] == 1 else 1
        self.colorize_next_move()

    def reset(self):
        for child in self.children:
            child.text = ''
            child.background_color = (1, 1, 1, 1)
        self.board_config['last'] = None
        self.board_config['won'] = {}

    def opponent_move(self):
        def move_to_win_board(sign):
            children = sorted(self.children, key=lambda x: x.position)
            n = self.next_available_moves()
            for move in n:
                for m, move_2 in moves.iteritems():
                    if move in move_2:
                        if self.board_config['won'].get(move_2, None) is None:
                            for con in conditions:
                                a = move_2[con[0] - 1]
                                b = move_2[con[1] - 1]
                                c = move_2[con[2] - 1]
                                d = children[a].text if a != move else sign
                                e = children[b].text if b != move else sign
                                f = children[c].text if c != move else sign
                                if (d == e == f) and d != '':
                                    return move
            return random.choice(n)

        next_pos = move_to_win_board(signs[self.board_config['current_sign']])
        for child in self.children:
            if child.position == next_pos:
                child.text = signs[self.board_config['current_sign']]
                self.board_config['last'] = child.position
        self.after_move()


class TheGame(App):
    title = 'Tic-Tac-Toe'

    @staticmethod
    def build():
        return TicTacToe()

    def on_start(self):
        GameConfig('Tryb gry')


if __name__ == '__main__':
    TheGame().run()
